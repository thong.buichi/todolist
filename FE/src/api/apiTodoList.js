import axiosClient from "./axiosClient";

const apiTodoList = {
  async getAll() {
    return await axiosClient.get("/todoList");
  },
  async addTodoList(formdata) {
    const dataTodoList = await axiosClient.post("/todoList", formdata);
    return dataTodoList;
  },
  async deleteTodoList(id) {
    const dataTodoList = await axiosClient.delete(`/todoList/${id}`);
    return dataTodoList;
  },
  async updateTodoList(idUpdate, formdata) {
    const dataTodoList = await axiosClient.put(
      `/todoList/${idUpdate}`,
      formdata
    );
    return dataTodoList;
  },
  async getTodoListById(id) {
    const dataTodoList = await axiosClient.get(`/todoList/${id}`);
    return dataTodoList;
  },
};

export default apiTodoList;

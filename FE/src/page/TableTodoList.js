import Button from "react-bootstrap/Button";
import React from "react";
import { Table } from "react-bootstrap";

const TableTodoList = ({ todoList, onRemove, onEdit }) => {
  const handleOnRemove = (id) => {
    onRemove(id);
  };
  const handleEdit = (id) => {
    onEdit(id);
  };

  return (
    <div className="px-[100px] w-[65%]">
      <Table className="" striped bordered hover>
        <thead className="bg-teal-800 border-black text-white">
          <tr>
            <th>Number</th>
            <th>Title</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {todoList.map((item, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td className="w-[250px]">{item.title}</td>
                <td>{item.gender}</td>
                <td>
                  <Button
                    onClick={() => {
                      handleOnRemove(item._id);
                    }}
                    className="bg-red-700"
                    variant="danger"
                  >
                    Delete
                  </Button>{" "}
                  <Button
                    onClick={() => {
                      handleEdit(item._id);
                    }}
                    className="bg-green-600"
                    variant="success"
                  >
                    Edit
                  </Button>{" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default TableTodoList;

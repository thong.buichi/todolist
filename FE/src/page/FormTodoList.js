import React, { useEffect, useState } from "react";
import { set, useForm } from "react-hook-form";
import { Button } from "../components/button";
import { Field } from "../components/field";
import { Input } from "../components/input";
import { Label } from "../components/label";
import { Select } from "../components/select";
import TableTodoList from "./TableTodoList";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import apiTodoList from "../api/apiTodoList";
import useLoading from "../hook/useLoading";

const schema = yup.object({
  title: yup.string().required("Please enter your title"),
});
const FormTodoList = () => {
  const [todoList, setTodoList] = useState([]);
  const [idUpdate, setIdUpdate] = useState("");

  const [loading, showLoader, hideLoader] = useLoading();
  const {
    handleSubmit,
    control,
    setValue,
    setError,
    formState: { errors },
  } = useForm({ mode: "onChange", resolver: yupResolver(schema) }); // <destructuring></destructuring>
  const onSubmit = async (values) => {
    if (idUpdate) {
      apiTodoList.updateTodoList(idUpdate, { ...values }).then((res) => {
        setIdUpdate("");
        getAllTodo();
      });
    } else {
      apiTodoList
        .addTodoList({ ...values })
        .then((res) => {
          getAllTodo();
        })
        .catch((res) => {});
    }
    setValue("title", "");
    setValue("gender", "female");
  };
  const handleOnRemove = (id) => {
    apiTodoList
      .deleteTodoList(id)
      .then((res) => {
        getAllTodo();
        setValue("title", "");
        setValue("gender", "female");
        setIdUpdate("");
        setError("title", "");
      })
      .catch((res) => {});
  };
  const handleEdit = (id) => {
    apiTodoList
      .getTodoListById(id)
      .then((res) => {
        setIdUpdate(res.data._id);
        setValue("title", res?.data?.title);
        setValue("gender", res?.data?.gender);
        setError("title", "");
      })
      .catch((res) => {});
  };
  useEffect(() => {
    getAllTodo();
  }, []);
  const getAllTodo = () => {
    showLoader();
    apiTodoList
      .getAll()
      .then((res) => {
        setTodoList(res.data);
        hideLoader();
        /* setTimeout(() => {
          setTodolist(res.data);
          hideLoader();
        }, 2000); */
      })
      .catch((res) => {});
  };

  return (
    <div className="w-full px-[100px] mt-[180px] flex">
      <form className="w-[35%]" onSubmit={handleSubmit(onSubmit)}>
        <Field>
          <Label htmlFor="title">Title</Label>
          <Input
            name="title"
            type="text"
            control={control}
            placeholder="Enter your title"
          ></Input>
          {errors?.title && (
            <div className="text-red-600">{errors?.title.message}</div>
          )}
        </Field>
        <Field>
          <Select
            name="gender"
            defaultValue="female"
            options={["female", "male", "other"]}
            control={control}
          />
        </Field>
        <Button type="submit">{idUpdate ? "Update" : "Submit"}</Button>
      </form>
      <TableTodoList
        todoList={todoList}
        onRemove={handleOnRemove}
        onEdit={handleEdit}
      />
      {loading}
    </div>
  );
};

export default FormTodoList;

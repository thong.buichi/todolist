import { useState } from "react";
import { Loading } from "../components/loading";

const useLoading = () => {
  const [loading, setLoading] = useState(false);
  return [
    loading ? <Loading /> : null,
    () => setLoading(true),
    () => setLoading(false),
  ];
};
export default useLoading;

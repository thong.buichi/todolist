import React from "react";
import { Spinner } from "react-bootstrap";

const Loading = () => {
  return (
    <div className="h-[100vh] w-[100vw] fixed top-0 left-0 bg-[#f8f8f8ad] flex justify-center items-center">
      <div className="h-10 w-10 rounded-full border-5 border-green-900 border-t-transparent border-t-4 mx-auto animate-spin"></div>
    </div>
  );
};

export default Loading;

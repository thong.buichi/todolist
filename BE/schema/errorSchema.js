const { body } = require("express-validator");
const validateSchema = {
  addValidateSchema: [
    body("title").notEmpty().withMessage("title cannot be empty"),
    body("gender").notEmpty().withMessage("gender cannot be empty"),
  ],
  putValidateSchema: [
    body("title").notEmpty().withMessage("title cannot be empty"),
    body("gender").notEmpty().withMessage("gender cannot be empty"),
  ],
};
module.exports = validateSchema;

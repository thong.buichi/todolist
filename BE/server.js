const express = require("express");
const app = express();
const port = 5000;
var bodyParser = require("body-parser");
const mongoose = require("mongoose");
const morgan = require("morgan");

const todoListRouter = require("./routes/todoListRoute");

var cors = require("cors");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan("common"));
app.use(cors());

app.use("/v1/todoList", todoListRouter);
//handle 404 not found
app.use((req, res) => {
  return res.send("404");
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

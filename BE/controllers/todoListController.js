const { TodoList } = require("../models");
const { validationResult } = require("express-validator");
const todoListController = {
  addTodoList: async (req, res) => {
    try {
      var title = req.body.title;
      var gender = req.body.gender;
      TodoList.create({
        title: title,
        gender: gender,
      });
      res.json("add successful!");
    } catch (error) {
      res.status(500).json(err);
    }
  },
  getTodoList: async (req, res) => {
    try {
      const todoList = await (await TodoList.find()).reverse();
      res.status(200).json(todoList);
    } catch (error) {
      res.status(500).json(error);
    }
  },
  updateTodoList: async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        res.status(400).json({ errors: errors.array() });
        return;
      }
      const todoList = await TodoList.findById(req.params.id);
      await todoList.updateOne({ $set: req.body });
      res.status(200).json("Update successfully");
    } catch (error) {
      res.status(500).json(error);
    }
  },
  deleteTodoList: async (req, res) => {
    try {
      const todoList = await TodoList.findByIdAndDelete(req.params.id);
      res.status(200).json("Delete successfully");
    } catch (error) {
      res.status(500).json(error);
    }
  },
  getTodoListId: async (req, res) => {
    try {
      const todoListId = await TodoList.findById(req.params.id);
      res.status(200).json(todoListId);
    } catch (error) {
      res.status(500).json(error);
    }
  },
};
module.exports = todoListController;

const todoListController = require("../controllers/todoListController");
const validateMiddleware = require("../middleware/validateMiddleware");
const validateSchema = require("../schema/errorSchema");

const route = require("express").Router();
route.post(
  "/",
  validateSchema.addValidateSchema,
  validateMiddleware.addValidate,
  todoListController.addTodoList
);
route.get("/", todoListController.getTodoList);
route.put(
  "/:id",
  validateSchema.putValidateSchema,
  validateMiddleware.putValidate,
  todoListController.updateTodoList
);
route.get("/:id", todoListController.getTodoListId);
route.delete("/:id", todoListController.deleteTodoList);
module.exports = route;

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/TodoList");
const todoListSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    required: true,
  },
  /*   time: { type: Date, default: Date.now }, */
});
const todoList = mongoose.model("todoList", todoListSchema);
module.exports = todoList;
